# Sportech test assignment

Minimum Python version is 3.5. Install requirements in a virtualenv with

    pip install -r requirements.txt

and run

    ./scraper_bot.py --help

for help.

Only one scraper is covered with tests due to the lack of time.
