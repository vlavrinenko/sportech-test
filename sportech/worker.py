import asyncio
import csv

from datetime import datetime
from fractions import Fraction
from statistics import mean

from terminaltables import AsciiTable

from .scrapers import (
    Bet365Scraper,
    PaddyPowerScraper,
    SkybetScraper,
    WilliamhillScraper,
)

scraper_classes = (
    WilliamhillScraper,
    SkybetScraper,
    PaddyPowerScraper,
    Bet365Scraper,
)


async def scrape_all():
    """Gather odds data from all the sites."""
    workers = [
        scrape_worker(klass)
        for klass in scraper_classes
    ]
    data = {}
    for future in asyncio.as_completed(workers):
        scraper_class, site_data = await future
        data[scraper_class] = site_data
    all_countries = set.union(*(set(d.keys()) for d in data.values()))
    result = [
        (country, tuple(
            data[scraper_class].get(country)
            for scraper_class in scraper_classes
        ))
        for country in all_countries
    ]
    result.sort(key=lambda item: mean(Fraction(x) for x in item[1] if x))
    return result


def format_result(data):
    """Generate rows formatted for output."""
    yield ('', ) + tuple(klass.label for klass in scraper_classes)
    for country, odds_list in data:
        yield (country, ) + tuple(
            '' if odds is None else odds for odds in odds_list
        )


async def scrape_worker(scraper_class):
    scraper = scraper_class()
    data = await scraper.scrape()
    return scraper_class, data


async def worker(
    loop: asyncio.AbstractEventLoop,
    output_file: str, interval: int, quiet: bool,
    first_call: bool = True
):
    if not first_call:
        await asyncio.sleep(interval)
    asyncio.ensure_future(worker(loop, output_file, interval, quiet, False))
    if not quiet:
        print('\n', datetime.now())
    result = await scrape_all()
    result_rows = list(format_result(result))
    if not quiet:
        table = AsciiTable(result_rows)
        print(table.table)
    if output_file is not None:
        with open(output_file, 'w') as f:
            writer = csv.writer(f)
            writer.writerows(result_rows)
