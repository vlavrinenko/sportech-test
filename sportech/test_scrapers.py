from unittest import TestCase

import bs4

from ..scrapers import WilliamhillScraper


class WilliamhillScraperTestCase(TestCase):

    def test_parser(self):
        # very minimal piece of html to test the scraper
        html = """
        <table class="tableData"><tr>
            <td>
                <div class="eventprice" id="ip_selection49641720price">
                    5/1
                </div>
                <div class="eventselection" id="ip_selection49641720name">
                    Germany
                </div>
            </td>
            <td>
                <div class="eventprice" id="ip_selection49641723price">
                    7/1
                </div>
                <div class="eventselection" id="ip_selection49641723name">
                    France
                </div>
            </td>
        </tr></table>
        """
        scraper = WilliamhillScraper()
        self.assertEqual(
            scraper.parse(bs4.BeautifulSoup(html, 'html.parser')),
            {'Germany': '5/1', 'France': '7/1'}
        )
