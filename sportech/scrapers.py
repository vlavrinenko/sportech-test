import re

from urllib.parse import urljoin, urlsplit
from urllib.request import getproxies

import aiohttp
import bs4


class BaseScraper:

    start_url: str = None  # override in descendant
    label: str = None  # override in descendant
    country_aliases = {}

    def __init__(self):
        self.odds_url = None

    async def scrape(self) -> dict:
        """Get the page and return {country: odds} dict."""
        try:
            async with aiohttp.ClientSession() as session:
                url = await self.get_odds_url(session)
                soup = await self.get_soup(session, url)
            return {
                self.country_aliases.get(country, country): odds
                for country, odds in self.parse(soup).items()
            }
        except:
            return {}

    async def get_soup(
        self, session: aiohttp.ClientSession, url: str
    ) -> bs4.BeautifulSoup:
        """Get URL contents and return it as a BeautifulSoup object."""
        url = urljoin(self.start_url, url)
        response = await session.get(url, proxy=self.get_proxy(url))
        html = await response.text()
        return bs4.BeautifulSoup(html, 'html.parser')

    async def get_odds_url(self, session: aiohttp.ClientSession) -> str:
        if self.odds_url is None:
            self.odds_url = await self.navigate(session)
        return self.odds_url

    async def navigate(self, session: aiohttp.ClientSession) -> str:
        """Go from home page and return the page with odds."""
        raise NotImplementedError()

    @staticmethod
    def get_proxy(url: str) -> str:
        schema, *_ = urlsplit(url)
        return getproxies().get(schema)


class WilliamhillScraper(BaseScraper):

    start_url = 'http://sports.williamhill.com'
    label = 'williamhill.com'

    async def navigate(self, session: aiohttp.ClientSession) -> str:
        soup = await self.get_soup(session, self.start_url)
        a_tag = soup.find('a', id='football')
        soup = await self.get_soup(session, a_tag['href'])
        a_tag = soup.find('a', string='World Cup 2018')
        soup = await self.get_soup(session, a_tag['href'])
        a_tag = soup.find(
            'a', string=re.compile(r'\s*World Cup 2018 - Outright\s*')
        )
        return a_tag['href']

    def parse(self, soup: bs4.BeautifulSoup) -> dict:
        def parse_div(div_tag: bs4.Tag):
            return re.search(r'\d+', div_tag['id'])[0], div_tag.string.strip()

        table_tag = soup.find('table', class_='tableData')
        name_tags = table_tag.find_all('div', class_='eventselection')
        name_map = {
            country_key: country
            for country_key, country in map(parse_div, name_tags)
        }
        odds_tags = table_tag.find_all('div', class_='eventprice')
        return {
            name_map[country_key]: odds
            for country_key, odds in map(parse_div, odds_tags)
        }


class SkybetScraper(BaseScraper):

    start_url = 'https://www.skybet.com/'
    label = 'skybet.com'
    country_aliases = {
        'Netherlands': 'Holland',
    }

    async def navigate(self, session: aiohttp.ClientSession) -> str:
        soup = await self.get_soup(session, self.start_url)
        a_tag = soup.find('a', string='Football')
        soup = await self.get_soup(session, a_tag['href'])
        a_tag = soup.find('a', string='World Cup 2018')
        return a_tag['href']

    def parse(self, soup: bs4.BeautifulSoup) -> dict:
        def parse_a(a_tag: bs4.Tag) -> tuple:
            price = '{0}/{1}'.format(
                a_tag['data-oc-price-num'], a_tag['data-oc-price-den']
            )
            return a_tag['data-oc-desc'], price

        odds_tags = soup.find_all('a', class_='oc oc-odds-desc')
        return {
            country: odds
            for country, odds in map(parse_a, odds_tags)
        }


class PaddyPowerScraper(BaseScraper):

    start_url = 'http://www.paddypower.com'
    label = 'paddypower.com'
    country_aliases = {
        'Bosnia': 'Bosnia-Herzegovina',
        'Ireland': 'Republic of Ireland',
    }

    async def navigate(self, session: aiohttp.ClientSession) -> str:
        soup = await self.get_soup(session, self.start_url)
        a_tag = soup.find('a', string='Football Betting')
        soup = await self.get_soup(session, a_tag['href'])
        a_tag = soup.find('a', string='Football Outrights')
        soup = await self.get_soup(session, a_tag['href'])
        a_tag = soup.find(
            string=re.compile(r'^[\s"]*World Cup 2018[\s"]*$')
        ).parent
        soup = await self.get_soup(session, a_tag['href'])
        a_tag = soup.find(
            'span', class_="tooltip", string=re.compile(r'Outright Betting')
        ).parent
        return a_tag['href']

    def parse(self, soup: bs4.BeautifulSoup) -> dict:
        def parse_a(a_tag: bs4.Tag) -> tuple:
            return (
                a_tag.find('span', class_='odds-label').string.strip(),
                a_tag.find('span', class_='odds-value').string,
            )

        odds_tags = soup.find_all('a', class_='fb-odds-button')
        return {
            country: odds
            for country, odds in map(parse_a, odds_tags)
        }


class Bet365Scraper(BaseScraper):

    start_url = 'https://mobile.bet365.com/'
    label = 'bet365.com'
    country_aliases = {
        'Rep of Ireland': 'Republic of Ireland',
    }

    async def navigate(self, session: aiohttp.ClientSession) -> str:
        # bet365.com is too AJAX-based; getting the needed URL
        # from its JS code doesn't look better than just hard-coding it.
        return (
            'https://mobile.bet365.com/V6/sport/coupon/coupon.aspx?'
            'zone=9&isocode=UA&tzi=1&key=1-172-1-26326924-2-0-0-0-2-'
            '0-0-0-0-0-1-0-0-0-0-0-0&ip=0&gn=0&cid=1&lng=1&ct=195&'
            'clt=9996&ot=1&pk=1'
        )

    def parse(self, soup: bs4.BeautifulSoup) -> dict:
        def parse_span(span_tag: bs4.Tag) -> tuple:
            return (
                str(span_tag.find(string=True, recursive=False)).strip(),
                span_tag.find('span', class_='odds').string
            )

        opp_tags = soup.find_all('span', class_='opp')
        return {
            country: odds
            for country, odds in map(parse_span, opp_tags)
        }
