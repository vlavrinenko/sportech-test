#!/usr/bin/env python3
"""Sportech scraper bot.

Usage: scaper_bot.py [options]

Oprions:
    --quiet, -q                Do not print results
    -o, --optput-file=FILE     Output file
    -n, --interval=SECONDS     Scraping interval [default: 300]
"""

import asyncio

from docopt import docopt

from sportech.worker import worker


def main(output_file: str, interval: int, quiet: bool):
    loop = asyncio.get_event_loop()
    asyncio.ensure_future(worker(loop, output_file, interval, quiet))
    loop.run_forever()


if __name__ == '__main__':
    options = docopt(__doc__)
    main(
        options['--optput-file'],
        int(options['--interval']),
        options['--quiet']
    )
